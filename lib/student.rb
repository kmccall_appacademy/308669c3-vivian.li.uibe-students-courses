class Student
  attr_reader :first_name, :last_name, :courses
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    [@first_name, @last_name].join(' ')
  end

  def enroll(new_el)
    unless @courses.include?(new_el) || conflict?(new_el)
      @courses << new_el
      new_el.students << self
    end
  end

  def course_load
    department_hash = Hash.new(0)
    courses.each do |el|
      department_hash[el.department] += el.credits
    end
    department_hash
  end

  def conflict?(new_course)
    @courses.any? do |old_course|
      raise 'error' if old_course.conflicts_with?(new_course)
    end
    false
  end
end
